# Python Package Template

This is a template to use when creating a python package.
Execute the following command to be sure you replaced everything that needs to:
```
/bin/grep -Rni code_template
/bin/grep -Rni class_template
/bin/grep -Rn \/USER
/bin/grep -Rni \/PATH
/bin/grep -Rni \@TAG
/bin/grep -Rni python_package_template
/bin/grep 'Python package description' setup.py
/bin/grep python_package setup.py
/bin/grep 'Some useful commentary on what this class is doing' python_package_template/code_template.py

```

## How to

1. Be sure to add library in requirements.txt
```
-e git+ssh://git@gitlab.com/USER/PATH/python_package_template@TAG#egg=python_package_template
```
2. Import and use
```
from python_package_template.code_template import class_template
class_template()
```
