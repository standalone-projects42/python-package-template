#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='python_package',
    version='1.0.0',
    author='dh4rm4',
    description='Python package description',
    install_requires=[],
    classifiers=[
        "Programming Language :: Python 3.8+",
        "Development Status :: Never ending",
        "Language :: English",
        "Operating System :: Debian based",
    ],
)
