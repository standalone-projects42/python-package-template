#!/usr/bin/python3
# -*- coding: utf-8 -*-


__all__ = ['class_template']


class class_template:
    """
        Some useful commentary on what this class is doing
    """
    def __init__(self):
        pass


if __name__ in '__main__':
    class_template()
